package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.UserId;

public interface PartnerRepInfoView {

	UserId getUserId();

	String getFirstName();

	String getLastName();

	boolean isBankInfoExists();

	boolean isW9InfoExists();

	boolean isContactInfoExists();

}
