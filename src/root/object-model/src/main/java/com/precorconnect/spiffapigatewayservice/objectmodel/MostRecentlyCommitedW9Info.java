package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.UserId;

public interface MostRecentlyCommitedW9Info {

	UserId getPartnerRepUserId();
	
	boolean isW9InfoExists();
}
