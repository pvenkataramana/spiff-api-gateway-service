package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface InvoiceInfo {

	InvoiceUrl getInvoiceUrl();
	
	InvoiceNumber getInvoiceNumber();
}
