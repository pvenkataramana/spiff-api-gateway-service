package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.PartnerSaleRegId;

public interface ClaimSpiffEntitlement {

	SpiffEntitlementId getSpiffEntitlementId();

	PartnerSaleRegId getPartnerSaleRegistrationId();

	InstallDate getInstallDate();

	SpiffAmount getSpiffAmount();

	PartnerRepInfoView getPartnerRepInfoView();

	InvoiceInfo getInvoiceInfo();

	FacilityName getFacilityName();

}
