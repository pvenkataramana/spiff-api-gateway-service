package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface PartnerSaleInvoiceId {

	Integer getValue();
	
}
