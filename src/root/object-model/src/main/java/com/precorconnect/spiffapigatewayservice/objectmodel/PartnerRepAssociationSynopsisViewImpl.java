package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.PartnerRepAssociationId;
import com.precorconnect.UserId;

public class PartnerRepAssociationSynopsisViewImpl
        implements PartnerRepAssociationSynopsisView {

    /*
    fields
     */
    private final PartnerRepAssociationId id;

    private final AccountId partnerId;

    private final UserId repId;

    /*
    constructors
     */
    public PartnerRepAssociationSynopsisViewImpl(
            @NonNull PartnerRepAssociationId id,
            @NonNull AccountId partnerId,
            @NonNull UserId repId
    ) {

        this.id =
                guardThat(
                        "id",
                        id
                )
                        .isNotNull()
                        .thenGetValue();

        this.partnerId =
                guardThat(
                        "partnerId",
                        partnerId
                )
                        .isNotNull()
                        .thenGetValue();

        this.repId =
                guardThat(
                        "repId",
                        repId
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public PartnerRepAssociationId getId() {
        return id;
    }

    @Override
    public AccountId getPartnerId() {
        return partnerId;
    }

    @Override
    public UserId getRepId() {
        return repId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        PartnerRepAssociationSynopsisViewImpl that = (PartnerRepAssociationSynopsisViewImpl) o;

        if (!id.equals(that.id)) {
			return false;
		}
        if (!partnerId.equals(that.partnerId)) {
			return false;
		}
        return repId.equals(that.repId);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + partnerId.hashCode();
        result = 31 * result + repId.hashCode();
        return result;
    }
}
