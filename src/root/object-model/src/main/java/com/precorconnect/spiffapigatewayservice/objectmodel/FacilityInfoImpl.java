package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class FacilityInfoImpl implements FacilityInfo  {
	
	private FacilityId facilityId;
	
	private FacilityName facilityName;
	
	public FacilityInfoImpl(@NonNull final FacilityId facilityid, @NonNull final FacilityName facilityName) {
		this.facilityId =  guardThat(
								"facilityId",
								facilityId
							).isNotNull()
							.thenGetValue();
		
		this.facilityName =  guardThat(
								"facilityName",
								facilityName
							).isNotNull()
							 .thenGetValue();

	}

	@Override
	public FacilityId getFacilityId() {
		return facilityId;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

}
