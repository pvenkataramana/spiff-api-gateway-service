package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface SpiffAmount {

	Double getValue();
}
