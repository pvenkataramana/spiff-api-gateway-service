package com.precorconnect.spiffapigatewayservice.objectmodel;


public final class InvoiceUrlImpl implements InvoiceUrl {

	private String invoiceUrl;

	public InvoiceUrlImpl(final String invoiceUrl) {
		this.invoiceUrl =  invoiceUrl;

	}

	@Override
	public String getValue() {
		return invoiceUrl;
	}

}
