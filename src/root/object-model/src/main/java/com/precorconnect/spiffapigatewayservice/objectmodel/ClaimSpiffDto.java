package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public interface ClaimSpiffDto {

	PartnerSaleRegId getPartnerSaleRegistrationId();

	AccountId getPartnerAccountId();

	UserId getPartnerRepUserId();

	SellDate getSellDate();

	InstallDate getInstallDate();

	SpiffAmount getSpiffAmount();

	SpiffClaimedDate getSpiffClaimedDate();

	FacilityName getFacilityName();

	InvoiceNumber getInvoiceNumber();

	SpiffEntitlementId getSpiffEntitlementId();

}
