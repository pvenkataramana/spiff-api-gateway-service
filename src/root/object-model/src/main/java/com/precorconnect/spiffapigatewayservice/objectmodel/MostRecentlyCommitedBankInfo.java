package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.UserId;

public interface MostRecentlyCommitedBankInfo {

	UserId getPartnerRepUserId();
	
	boolean isBankInfoExists();
}
