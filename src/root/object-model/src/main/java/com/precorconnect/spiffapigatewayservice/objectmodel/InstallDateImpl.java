package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class InstallDateImpl implements InstallDate {
	
	private Date installDate;
	
	public InstallDateImpl(@NonNull final Date installDate) {
		
		this.installDate =  guardThat(
								"installDate",
								installDate
							).isNotNull()
							 .thenGetValue();
	}

	@Override
	public Date getValue() {
		return installDate;
	}

}
