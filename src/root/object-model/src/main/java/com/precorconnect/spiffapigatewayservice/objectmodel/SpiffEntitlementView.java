package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;


public interface SpiffEntitlementView {

	SpiffEntitlementId getSpiffEntitlementId();

	PartnerSaleRegId getPartnerSaleRegistrationId();

	InstallDate getInstallDate();

	SpiffAmount getSpiffAmount();

	UserId getPartnerRepuserId();

	InvoiceInfo getInvoiceInfo();

	FacilityName getFacilityName();

	SellDate getSellDate();

}
