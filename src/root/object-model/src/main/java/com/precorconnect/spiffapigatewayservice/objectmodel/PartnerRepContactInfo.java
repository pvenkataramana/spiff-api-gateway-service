package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.UserId;

public interface PartnerRepContactInfo {

	UserId getPartnerRepUserId();
	
	boolean isContactInfoExists();
}
