package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface InvoiceUrl {

	String getValue();
}
