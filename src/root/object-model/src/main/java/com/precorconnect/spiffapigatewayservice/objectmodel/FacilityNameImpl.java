package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public final class FacilityNameImpl implements FacilityName {
	
	private String facilityName;
	
	public FacilityNameImpl(@NonNull final String facilityName) {
		this.facilityName =  guardThat(
									"facilityName",
									facilityName
							 ).isNotNull()
							  .thenGetValue();
	}

	@Override
	public String getValue() {
		return facilityName;
	}

}
