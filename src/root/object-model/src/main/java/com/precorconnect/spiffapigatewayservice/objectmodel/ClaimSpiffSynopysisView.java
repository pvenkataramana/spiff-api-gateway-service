package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public interface ClaimSpiffSynopysisView {

	  ClaimSpiffId getClaimId();

	  PartnerSaleRegId getPartnerSaleRegistrationId();

      AccountId getPartnerAccountId();

      UserId getPartnerRepUserId();

      InstallDate getInstallDate();

      SpiffAmount getSpiffAmount();

      SpiffClaimedDate getSpiffClaimedDate();

      FacilityName getFacilityName();

      InvoiceNumber getInvoiceNumber();

}