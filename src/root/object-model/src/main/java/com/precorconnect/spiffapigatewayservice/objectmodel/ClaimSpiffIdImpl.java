package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

public final class ClaimSpiffIdImpl implements ClaimSpiffId {

	private Long value;
	
	public ClaimSpiffIdImpl(final Long value) {
	    this.value =
	            guardThat(
	                    "claimSpiffId",
	                    value
	            )
	                    .isNotNull()
	                    .thenGetValue();
	}
	


	@Override
	public Long getvalue() {
		return value;
	}

}
