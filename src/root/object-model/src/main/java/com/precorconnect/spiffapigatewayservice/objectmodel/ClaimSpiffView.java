package com.precorconnect.spiffapigatewayservice.objectmodel;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public interface ClaimSpiffView {

	  ClaimSpiffId getClaimId();

	  PartnerSaleRegId getPartnerSaleRegistrationId();

      AccountId getPartnerAccountId();

      UserId getPartnerRepUserId();

      FirstName getPartnerRepFirstName();

      LastName getPartnerRepLastName();

      InstallDate getInstallDate();

      SpiffAmount getSpiffAmount();

      SpiffClaimedDate getSpiffClaimedDate();

      FacilityName getFacilityName();

      InvoiceNumber getInvoiceNumber();

}