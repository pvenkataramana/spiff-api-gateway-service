package com.precorconnect.spiffapigatewayservice.objectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.UserId;

public final class SpiffEntitlementViewImpl implements SpiffEntitlementView {

	private SpiffEntitlementId spiffEntitlementId;

	private PartnerSaleRegId partnerSaleRegistrationId;

	private InstallDate installDate;

	private SpiffAmount spiffAmount;

	private UserId partnerRepUserId;

	private InvoiceInfo invoiceInfo;

	private FacilityName facilityName;

	private SellDate sellDate;

	public SpiffEntitlementViewImpl(@NonNull final PartnerSaleRegId partnerSaleRegistrationId,
							  @NonNull final InstallDate installDate,
							  @NonNull final SpiffAmount spiffAmount,
							  final UserId partnerRepUserId,
							  @NonNull final InvoiceInfo invoiceInfo,
							  @NonNull final FacilityName facilityName,
							  @NonNull final SpiffEntitlementId spiffEntitlementId,
							  @NonNull final SellDate sellDate) {

		this.partnerSaleRegistrationId =  guardThat(
        										"partnerSaleRegistrationId",
        										partnerSaleRegistrationId
												).isNotNull()
												 .thenGetValue();

		this.installDate =  guardThat(
										"installDate",
										installDate
									).isNotNull()
									 .thenGetValue();

		this.spiffAmount =  guardThat(
									"spiffAmount",
									spiffAmount
								).isNotNull()
								 .thenGetValue();

		this.partnerRepUserId = partnerRepUserId;


		this.invoiceInfo =  guardThat(
									"invoiceInfo",
									invoiceInfo
							    ).isNotNull()
							     .thenGetValue();


		this.facilityName = guardThat(
									"facilityName",
									facilityName
								).isNotNull()
								.thenGetValue();

		this.spiffEntitlementId = guardThat(
									"spiffEntitlementId",
									spiffEntitlementId
								).isNotNull()
								 .thenGetValue();

		this.sellDate =  guardThat(
								"sellDate",
								sellDate
							).isNotNull()
							.thenGetValue();

	}

	@Override
	public PartnerSaleRegId getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	@Override
	public InstallDate getInstallDate() {
		return installDate;
	}

	@Override
	public SpiffAmount getSpiffAmount() {
		return spiffAmount;
	}

	@Override
	public UserId getPartnerRepuserId() {
		return partnerRepUserId;
	}

	@Override
	public InvoiceInfo getInvoiceInfo() {
		return invoiceInfo;
	}

	@Override
	public FacilityName getFacilityName() {
		return facilityName;
	}

	@Override
	public SpiffEntitlementId getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	@Override
	public SellDate getSellDate() {
		return sellDate;
	}
}
