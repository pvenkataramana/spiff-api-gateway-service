package com.precorconnect.spiffapigatewayservice.objectmodel;

import java.net.URL;

import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleRegId;

public interface PartnerSaleInvoiceView {

    PartnerSaleInvoiceId getId();

    PartnerSaleInvoiceNumber getNumber();

    URL getFileUrl();

    PartnerSaleRegId getPartnerSaleRegId();

}
