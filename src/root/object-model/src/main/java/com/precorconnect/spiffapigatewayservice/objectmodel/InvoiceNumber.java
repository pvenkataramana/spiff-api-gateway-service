package com.precorconnect.spiffapigatewayservice.objectmodel;

public interface InvoiceNumber {

	String getValue();
}
