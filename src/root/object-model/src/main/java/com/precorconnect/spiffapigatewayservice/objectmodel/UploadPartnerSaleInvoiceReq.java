package com.precorconnect.spiffapigatewayservice.objectmodel;

import java.io.File;

import com.precorconnect.PartnerSaleInvoiceNumber;
import com.precorconnect.PartnerSaleRegId;

public interface UploadPartnerSaleInvoiceReq {

    PartnerSaleRegId getPartnerSaleRegId();

    PartnerSaleInvoiceNumber getNumber();

    File getFile();
}
