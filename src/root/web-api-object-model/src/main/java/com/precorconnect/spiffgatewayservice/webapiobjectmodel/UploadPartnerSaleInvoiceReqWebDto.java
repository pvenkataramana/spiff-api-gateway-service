package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.io.File;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UploadPartnerSaleInvoiceReqWebDto {

	/*
	 fields
	 */
	private final Long partnerSaleRegId;

    private final String partnerSaleInvoiceNumber;

    private final File file;

    public UploadPartnerSaleInvoiceReqWebDto(){

    	partnerSaleRegId = 0L;

    	partnerSaleInvoiceNumber = null;

    	file = null;
    }

    public UploadPartnerSaleInvoiceReqWebDto(
    		@NonNull final Long partnerSaleRegId,
    		@NonNull final String partnerSaleInvoiceNumber,
    		@NonNull final File file
    		){

    	this.partnerSaleRegId =
                guardThat(
                        "partnerSaleRegId",
                        partnerSaleRegId
                )
                        .isNotNull()
                        .thenGetValue();

    	this.partnerSaleInvoiceNumber =
                guardThat(
                        "partnerSaleInvoiceNumber",
                        partnerSaleInvoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    	this.file =
                guardThat(
                        "file",
                        file
                )
                        .isNotNull()
                        .thenGetValue();

    }

	public Long getPartnerSaleRegId() {
		return partnerSaleRegId;
	}

	public String getPartnerSaleInvoiceNumber() {
		return partnerSaleInvoiceNumber;
	}

	public File getFile() {
		return file;
	}


}
