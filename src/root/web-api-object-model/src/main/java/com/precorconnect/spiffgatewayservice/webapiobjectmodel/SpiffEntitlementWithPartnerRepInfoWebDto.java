package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffEntitlementWithPartnerRepInfoWebDto {
	/*
	 fields
	 */

	private final Long spiffEntitlementId;

	private final Long partnerSaleRegistrationId;

	private final String installDate;

	private final Double spiffAmount;

	private final String partnerRepUserId;

	private final String invoiceUrl;

	private final String invoiceNumber;

	private final String facilityName;

	private final String sellDate;

	/*
	 Constructors
	 */
	public SpiffEntitlementWithPartnerRepInfoWebDto(){

		spiffEntitlementId = 0L;

		partnerSaleRegistrationId = 0L;

		installDate = null;

		spiffAmount = 0.00;

		partnerRepUserId = null;

		invoiceUrl = null;

		invoiceNumber = null;

		facilityName = null;

		sellDate = null;

	}

	public SpiffEntitlementWithPartnerRepInfoWebDto(
			@NonNull Long spiffEntitlementId,
			@NonNull Long partnerSaleRegistrationId,
			@NonNull String installDate,
			@NonNull Double spiffAmount,
			@NonNull String partnerRepUserId,
			@NonNull String invoiceUrl,
			@NonNull String invoiceNumber,
			@NonNull String facilityName,
			@NonNull String sellDate
			){

		this.spiffEntitlementId =
               guardThat(
                       "spiffEntitlementId",
                       spiffEntitlementId
               )
                       .isNotNull()
                       .thenGetValue();

		this.partnerSaleRegistrationId =
               guardThat(
                       "partnerSaleRegistrationId",
                       partnerSaleRegistrationId
               )
                       .isNotNull()
                       .thenGetValue();

		this.installDate =
               guardThat(
                       "installDate",
                       installDate
               )
                       .isNotNull()
                       .thenGetValue();

		this.spiffAmount =
               guardThat(
                       "spiffAmount",
                       spiffAmount
               )
                       .isNotNull()
                       .thenGetValue();

		this.partnerRepUserId =
               guardThat(
                       "partnerRepUserId",
                       partnerRepUserId
               )
                       .isNotNull()
                       .thenGetValue();

		this.invoiceUrl =
               guardThat(
                       "invoiceUrl",
                       invoiceUrl
               )
                       .isNotNull()
                       .thenGetValue();

		this.invoiceNumber =
               guardThat(
                       "invoiceNumber",
                       invoiceNumber
               )
                       .isNotNull()
                       .thenGetValue();

		this.facilityName =
               guardThat(
                       "facilityName",
                       facilityName
               )
                       .isNotNull()
                       .thenGetValue();

		this.sellDate =
	               guardThat(
	                       "sellDate",
	                       sellDate
	               )
	                       .isNotNull()
	                       .thenGetValue();
	}

	/*
	 getter methods
	 */

	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getSellDate(){
		return sellDate;
	}
}
