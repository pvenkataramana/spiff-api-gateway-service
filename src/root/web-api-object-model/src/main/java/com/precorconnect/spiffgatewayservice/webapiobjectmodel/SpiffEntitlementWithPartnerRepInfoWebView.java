package com.precorconnect.spiffgatewayservice.webapiobjectmodel;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class SpiffEntitlementWithPartnerRepInfoWebView {

	/*
	 fields
	 */

	private final Long spiffEntitlementId;

	private final Long partnerSaleRegistrationId;

	private final String installDate;

	private final Double spiffAmount;

	private final String partnerRepUserId;

	private final String firstName;

	private final String lastName;

	private final boolean isBankInfoExists;

	private final boolean isW9InfoExists;

	private final boolean isContactInfoExists;

	private final String invoiceUrl;

	private final String invoiceNumber;

	private final String facilityName;

	private final String sellDate;

	/*
	 Constructors
	 */
	public SpiffEntitlementWithPartnerRepInfoWebView(){

		spiffEntitlementId = 0L;

		partnerSaleRegistrationId = 0L;

		installDate = null;

		spiffAmount = 0.00;

		partnerRepUserId = null;

		firstName = null;

		lastName = null;

		isBankInfoExists = false;

		isW9InfoExists = false;

		isContactInfoExists = false;

		invoiceUrl = null;

		invoiceNumber = null;

		facilityName = null;

		sellDate = null;

	}

	public SpiffEntitlementWithPartnerRepInfoWebView(
			@NonNull Long spiffEntitlementId,
			@NonNull Long partnerSaleRegistrationId,
			@NonNull String installDate,
			@NonNull Double spiffAmount,
			String partnerRepUserId,
			String firstName,
			String lastName,
			@NonNull boolean isBankInfoExists,
			@NonNull boolean isW9InfoExists,
			@NonNull boolean isContactInfoExists,
			String invoiceUrl,
			@NonNull String invoiceNumber,
			@NonNull String facilityName,
			@NonNull String sellDate
			){

		this.spiffEntitlementId =
                guardThat(
                        "spiffEntitlementId",
                        spiffEntitlementId
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerSaleRegistrationId =
                guardThat(
                        "partnerSaleRegistrationId",
                        partnerSaleRegistrationId
                )
                        .isNotNull()
                        .thenGetValue();

		this.installDate =
                guardThat(
                        "installDate",
                        installDate
                )
                        .isNotNull()
                        .thenGetValue();

		this.spiffAmount =
                guardThat(
                        "spiffAmount",
                        spiffAmount
                )
                        .isNotNull()
                        .thenGetValue();

		this.partnerRepUserId = partnerRepUserId;

		this.firstName = firstName;

		this.lastName = lastName;

		this.isBankInfoExists = isBankInfoExists;

		this.isW9InfoExists = isW9InfoExists;

		this.isContactInfoExists = isContactInfoExists;

		this.invoiceUrl = invoiceUrl;

		this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

		this.facilityName =
                guardThat(
                        "facilityName",
                        facilityName
                )
                        .isNotNull()
                        .thenGetValue();

		this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();
	}

	/*
	 getter methods
	 */

	public Long getSpiffEntitlementId() {
		return spiffEntitlementId;
	}

	public Long getPartnerSaleRegistrationId() {
		return partnerSaleRegistrationId;
	}

	public String getInstallDate() {
		return installDate;
	}

	public Double getSpiffAmount() {
		return spiffAmount;
	}

	public String getPartnerRepUserId() {
		return partnerRepUserId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public boolean isBankInfoExists() {
		return isBankInfoExists;
	}

	public boolean isW9InfoExists() {
		return isW9InfoExists;
	}

	public boolean isContactInfoExists() {
		return isContactInfoExists;
	}

	public String getInvoiceUrl() {
		return invoiceUrl;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public String getSellDate() {
		return sellDate;
	}

}
