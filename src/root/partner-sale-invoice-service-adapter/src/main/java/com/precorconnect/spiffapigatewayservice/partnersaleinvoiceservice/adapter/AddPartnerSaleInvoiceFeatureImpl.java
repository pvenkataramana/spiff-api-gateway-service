package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleInvoiceId;
import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public final class AddPartnerSaleInvoiceFeatureImpl implements AddPartnerSaleInvoiceFeature {

	private final PartnerSaleInvoiceServiceSdk partnerSaleInvoiceServiceSdk;


	@Inject
	public AddPartnerSaleInvoiceFeatureImpl(final PartnerSaleInvoiceServiceSdk partnerSaleInvoiceServiceSdk) {
		this.partnerSaleInvoiceServiceSdk = guardThat(
                "partnerSaleInvoiceServiceSdk",
                partnerSaleInvoiceServiceSdk
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public PartnerSaleInvoiceId execute(
			@NonNull UploadPartnerSaleInvoiceReq request,
			@NonNull OAuth2AccessToken oAuth2AccessToken)
			throws AuthenticationException, AuthorizationException {



		return partnerSaleInvoiceServiceSdk.addPartnerSaleInvoice(
																request.getNumber().getValue(),
																request.getPartnerSaleRegId().getValue(),
																request.getFile(),
																oAuth2AccessToken
																);
	}

}
