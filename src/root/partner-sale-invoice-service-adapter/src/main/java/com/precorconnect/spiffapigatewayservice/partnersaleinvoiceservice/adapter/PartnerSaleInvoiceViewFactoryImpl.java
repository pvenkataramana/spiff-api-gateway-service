package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import com.precorconnect.PartnerSaleInvoiceIdImpl;
import com.precorconnect.PartnerSaleInvoiceNumberImpl;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceViewImpl;

public final class PartnerSaleInvoiceViewFactoryImpl implements PartnerSaleInvoiceViewFactory {

	@Override
	public PartnerSaleInvoiceView construct(
			com.precorconnect.partnersaleinvoiceservice.webapi.PartnerSaleInvoiceView partnerSaleInvoiceView) {

		return new PartnerSaleInvoiceViewImpl(new PartnerSaleInvoiceIdImpl(partnerSaleInvoiceView.getId()),
											new PartnerSaleInvoiceNumberImpl(partnerSaleInvoiceView.getNumber()) ,
											partnerSaleInvoiceView.getFileUrl(),
											new PartnerSaleRegIdImpl(partnerSaleInvoiceView.getPartnerSaleRegId().get()));
	}

}
