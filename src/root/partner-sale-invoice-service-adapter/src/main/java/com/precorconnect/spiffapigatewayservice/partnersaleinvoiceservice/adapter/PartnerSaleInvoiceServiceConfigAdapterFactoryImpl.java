package com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.partnersaleinvoiceservice.sdk.PartnerSaleInvoiceServiceSdkConfigImpl;

public final class PartnerSaleInvoiceServiceConfigAdapterFactoryImpl implements
		PartnerSaleInvoiceServiceAdapterConfigFactory {

	@Override
	public PartnerSaleInvoiceServiceAdapterConfig construct() {
		return new PartnerSaleInvoiceServiceAdapterConfigImpl(
				new PartnerSaleInvoiceServiceSdkConfigImpl(
						constructPrecorConnectApiBaseUrl()));
	}

	private URL constructPrecorConnectApiBaseUrl() {

		String precorConnectApiBaseUrl = System
				.getenv("PRECOR_CONNECT_API_BASE_URL");

		try {

			return new URL(precorConnectApiBaseUrl);

		} catch (MalformedURLException e) {

			throw new RuntimeException(e);

		}
	}

}
