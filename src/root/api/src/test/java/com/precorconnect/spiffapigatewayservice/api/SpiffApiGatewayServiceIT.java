package com.precorconnect.spiffapigatewayservice.api;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;

public class SpiffApiGatewayServiceIT {

	/**
	 *
	 */

	private Dummy dummy=new Dummy();

	private Config config=
			new ConfigFactory().construct();

	 private final Factory factory =

	    		new Factory(
	                    new IdentityServiceIntegrationTestSdkImpl(
	                    		config.getIdentityServiceJwtSigningKey()
	                    ),
	                    dummy
	            );



	private SpiffApiGatewayServiceImpl SpiffApiGatewayServiceConfigFactoryImpl=
										new SpiffApiGatewayServiceImpl(
												config
												.getSpiffApiGatewayServiceConfig()
												);

	/**
	 * test methods
	 */

		@Test
	public void testListPartnerRepsInfo_withAccountId_showsPartnerRepViewList(
														) throws AuthenticationException, AuthorizationException{

		Collection<PartnerRepInfoView> partnerRepInfoViewList=
									SpiffApiGatewayServiceConfigFactoryImpl
									.listPartnerRepsInfoWithAccountId(
											dummy.getAccountId(),
											factory.constructValidAppOAuth2AccessToken()
									);

		assertThat(
				partnerRepInfoViewList
				.size()
			)
			.isGreaterThan(0);

	}

	@Test
	public void testListSpiffEntitlements_withAccountId_showsSpiffEntitlementWithPartnerRepInfoViewList(
																				) throws AuthenticationException, AuthorizationException{

		Collection<SpiffEntitlementWithPartnerRepInfoView> spiffEntitlementWithPartnerRepInfoViewList=
													SpiffApiGatewayServiceConfigFactoryImpl
													.listSpiffEntitlementsAccountId(
															dummy.getAccountId(),
															factory.constructValidAppOAuth2AccessToken()
														);

		assertThat(
				spiffEntitlementWithPartnerRepInfoViewList
				.size()
			)
			.isGreaterThan(0);
	}

	@Test
	public void uploadInvoiceForPartnerSaleRegistration(
						   ) throws AuthenticationException, AuthorizationException{

		PartnerSaleInvoiceView partnerSaleInvoiceView=
						SpiffApiGatewayServiceConfigFactoryImpl
						.uploadInvoiceForPartnerSaleRegistration(
								dummy.getUploadPartnerSaleInvoiceReq(),
								factory.constructValidAppOAuth2AccessToken()
						);

		assertEquals(
				dummy.getPartnerRepInfoView(),
				partnerSaleInvoiceView
				);


	}

	@Ignore
	@Test
	public void claimSpiffEntitlements(
			) throws AuthenticationException, AuthorizationException{

		Collection<ClaimSpiffView> claimSpiffViewList=
							SpiffApiGatewayServiceConfigFactoryImpl
							.claimSpiffEntitlements(
							dummy.getAccountId(),
							dummy.getSpiffEntitlements(),
							factory.constructValidAppOAuth2AccessToken()
						);

		assertEquals(
				dummy.getClaimSpiffViewList().size(),
				claimSpiffViewList.size()
				);

	}

}
