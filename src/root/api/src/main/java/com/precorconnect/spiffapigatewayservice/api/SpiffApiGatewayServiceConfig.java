package com.precorconnect.spiffapigatewayservice.api;

import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ClaimSpiffsServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter.PartnerRepAssociationServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter.PartnerRepServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.partnersaleinvoiceservice.adapter.PartnerSaleInvoiceServiceAdapterConfig;



public interface SpiffApiGatewayServiceConfig {

	PartnerRepServiceAdapterConfig getPartnerRepServiceAdapterConfig();

	PartnerRepAssociationServiceAdapterConfig getPartnerRepAssociationServiceAdapterConfig();

	PartnerSaleInvoiceServiceAdapterConfig getPartnerSaleInvoiceServiceAdapterConfig();

	ClaimSpiffsServiceAdapterConfig getClaimSpiffsServiceAdapterConfig();
}
