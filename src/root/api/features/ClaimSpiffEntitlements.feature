Feature: Claim Spiff Entitlements
  Claim Spiff Entitlements
  
  Background:
    Given a partnerSaleRegistrationId and invoiceUrl consists of:
      | attribute     								            | validation | type   |
      | Collection<SpiffEntitlementWithPartnerRepInfoWebDto>    | required   | object |
      | accountId                      				            | required   | string |

  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid Collection<SpiffEntitlementWithPartnerRepInfoWebDto> and accountId
    When I execute claimSpiffEntitlements
    Then the Collection of ClaimSpiffWebView is returned