Feature: List Partner Rep With AccountId
  List Partner Rep With AccountId

  Background:
    Given a accountId consists of:
      | attribute     				| validation | type   |
      | accountId     				| required   | string |
      
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid accountId
    When I execute listPartnerRepWithAccountId
    Then the collection of PartnerRepInfoWebView with the matched accountid are returned
