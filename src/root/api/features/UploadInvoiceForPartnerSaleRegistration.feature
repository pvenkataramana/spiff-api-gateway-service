Feature: Upload Invoice For PartnerSaleRegistration
  Upload Invoice For PartnerSaleRegistration

 Background:
    Given a partnerSaleInvoiceNumber, partnerSaleRegId and file consists of:
      | attribute     			   | validation | type        |
      | partnerSaleInvoiceNumber   | required   | string      |
      | partnerSaleRegId           | optional   | number      | 
      | file					   | required   |MultipartFile|

  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid partnerSaleInvoiceNumber , partnerSaleRegId and file
    When I execute uploadInvoiceForPartnerSaleRegistration
    Then the PartnerSaleInvoiceWebView returned