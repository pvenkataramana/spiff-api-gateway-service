package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.PartnerSaleRegId;

public interface PartnerSaleRegistrationIdFactory {

	com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId construct(
			PartnerSaleRegId partnerSaleRegid);
}
