package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffIdImpl;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;

public final class GetClaimSpiffsWithIdsFeatureImpl implements
		GetClaimSpiffsWithIdsFeature {
	private final ClaimSpiffServiceSdk claimSpiffServiceSdk;

	private final ClaimSpiffViewFactory claimSpiffViewFactory;

	@Inject
	public GetClaimSpiffsWithIdsFeatureImpl(
			final ClaimSpiffServiceSdk claimSpiffServiceSdk,
			final ClaimSpiffViewFactory claimSpiffViewFactory) {
		this.claimSpiffServiceSdk = guardThat("claimSpiffServiceSdk",
				claimSpiffServiceSdk).isNotNull().thenGetValue();
		;

		this.claimSpiffViewFactory = guardThat("claimSpiffViewFactory",
				claimSpiffViewFactory).isNotNull().thenGetValue();
		;

	}

	@Override
	public Collection<ClaimSpiffSynopysisView> execute(
			@NonNull Collection<ClaimSpiffId> gatewayClaimSpiffIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
		List<com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffId> transformedClaimSpiffIds = gatewayClaimSpiffIds
				.stream()
				.map(claimSpiffId -> new ClaimSpiffIdImpl(claimSpiffId.getvalue()))
				.collect(Collectors.toList());

		Collection<ClaimSpiffSynopysisView> gatewayClaimSpiffs = new ArrayList<ClaimSpiffSynopysisView>();

		gatewayClaimSpiffs.addAll(claimSpiffServiceSdk
				.getClaimSpiffsWithIds(transformedClaimSpiffIds, accessToken)
				.stream().map(claimSpiffViewFactory::construct)
				.collect(Collectors.toList()));

		return gatewayClaimSpiffs;
	}

}
