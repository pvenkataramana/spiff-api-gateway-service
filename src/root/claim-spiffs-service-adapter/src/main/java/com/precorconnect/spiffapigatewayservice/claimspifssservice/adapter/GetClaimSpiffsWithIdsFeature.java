package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;

public interface GetClaimSpiffsWithIdsFeature {
	 Collection<ClaimSpiffSynopysisView> execute(@NonNull Collection<ClaimSpiffId> gatewayClaimSpiffIds,
				@NonNull OAuth2AccessToken accessToken) throws AuthenticationException;

}
