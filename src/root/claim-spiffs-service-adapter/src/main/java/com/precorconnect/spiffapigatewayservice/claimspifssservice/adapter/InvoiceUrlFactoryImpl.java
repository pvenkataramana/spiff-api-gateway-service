package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;

public final class InvoiceUrlFactoryImpl implements InvoiceUrlFactory {

	@Override
	public InvoiceUrl construct(com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl invoiceUrl) {
		return new InvoiceUrlImpl(invoiceUrl.getValue());
	}

}
