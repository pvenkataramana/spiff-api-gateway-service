package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SellDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.SellDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementViewImpl;

public final class SpiffEntitlementViewFactoryImpl implements SpiffEntitlementViewFactory {

	@Override
	public SpiffEntitlementView construct(
			com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView spiffEntitlementView) {

		UserId userId = spiffEntitlementView.getPartnerRepUserId() != null ? new UserIdImpl(spiffEntitlementView.getPartnerRepUserId().getValue()): null;

		return new SpiffEntitlementViewImpl(convert(spiffEntitlementView.getPartnerSaleRegistrationId()),
											convert(spiffEntitlementView.getInstallDate()),
											convert(spiffEntitlementView.getSpiffAmount()),
											userId,
											convert(spiffEntitlementView.getInvoiceUrl(), spiffEntitlementView.getInvoiceNumber()),
											convert(spiffEntitlementView.getFacilityName()),
											convert(spiffEntitlementView.getSpiffEntitlementId()),
											convert(spiffEntitlementView.getSellDate()));
	}

	private PartnerSaleRegId convert(final PartnerSaleRegistrationId partnerSaleRegId) {
		return new PartnerSaleRegIdImpl(partnerSaleRegId.getValue());
	}

	private InstallDate convert(final com.precorconnect.claimspiffservice.objectmodel.InstallDate installDate) {
		return new InstallDateImpl(installDate.getValue());

	}

	private SpiffAmount convert(final com.precorconnect.claimspiffservice.objectmodel.SpiffAmount spiffAmount) {
		return new SpiffAmountImpl(spiffAmount.getValue());

	}


	private InvoiceInfo convert(final com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl invoiceUrl,
			final com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber invoiceNumber) {
		return new InvoiceInfoImpl(new InvoiceUrlImpl(invoiceUrl.getValue()), new InvoiceNumberImpl(invoiceNumber.getValue()));
	}

	private FacilityName convert(final com.precorconnect.claimspiffservice.objectmodel.FacilityName facilityName) {
		return new FacilityNameImpl(facilityName.getValue());

	}

	private SpiffEntitlementId convert(final com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementId spiffEntitlementId) {
		return new SpiffEntitlementIdImpl(spiffEntitlementId.getValue());

	}

	private SellDate convert(final com.precorconnect.claimspiffservice.objectmodel.SellDate sellDate) {
		return new SellDateImpl(sellDate.getValue());

	}


}
