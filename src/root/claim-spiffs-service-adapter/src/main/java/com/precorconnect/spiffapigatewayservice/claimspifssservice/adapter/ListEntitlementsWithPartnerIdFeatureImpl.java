package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public final class ListEntitlementsWithPartnerIdFeatureImpl implements ListEntitlementsWithPartnerIdFeature {

	private ClaimSpiffServiceSdk claimSpiffServiceSdk;

	private SpiffEntitlementViewFactory spiffEntitlementViewFactory;
	private static final Logger LOGGER = LoggerFactory.getLogger(ListEntitlementsWithPartnerIdFeatureImpl.class);

	@Inject
	public ListEntitlementsWithPartnerIdFeatureImpl(final ClaimSpiffServiceSdk claimSpiffServiceSdk,
									   final SpiffEntitlementViewFactory spiffEntitlementViewFactory) {
		this.claimSpiffServiceSdk = guardThat(
                "claimSpiffServiceSdk",
                claimSpiffServiceSdk
			  ).isNotNull()
			   .thenGetValue();

		this.spiffEntitlementViewFactory = guardThat(
                "spiffEntitlementViewFactory",
                spiffEntitlementViewFactory
			  ).isNotNull()
			   .thenGetValue();


	}


	@Override
	public Collection<SpiffEntitlementView> execute(@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		Collection<SpiffEntitlementView> spiffEntitlements = new ArrayList<SpiffEntitlementView>();

		spiffEntitlements.addAll(claimSpiffServiceSdk.listEntitlementsWithPartnerId(partnerAccountId, accessToken)
								   .stream()
								   .map(spiffEntitlementViewFactory::construct)
								   .collect(Collectors.toList()));
		LOGGER.debug(String.format("got %s spiffEntitlements", spiffEntitlements.size()));
		return spiffEntitlements;
	}

}
