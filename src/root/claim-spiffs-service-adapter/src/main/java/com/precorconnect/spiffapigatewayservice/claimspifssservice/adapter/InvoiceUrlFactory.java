package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;

public interface InvoiceUrlFactory {

	InvoiceUrl construct(com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl invoiceUrl);
}
