package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkConfig;

public final class ClaimSpiffsServiceAdapterConfigImpl implements ClaimSpiffsServiceAdapterConfig {

	private final ClaimSpiffServiceSdkConfig claimSpiffServiceSdkConfig;
	
	public ClaimSpiffsServiceAdapterConfigImpl(final ClaimSpiffServiceSdkConfig claimSpiffServiceSdkConfig) {
		this.claimSpiffServiceSdkConfig = guardThat(
                "claimSpiffServiceSdkConfig",
                claimSpiffServiceSdkConfig
			  ).isNotNull()
			   .thenGetValue();;

	}
	
	@Override
	public ClaimSpiffServiceSdkConfig getClaimSpiffsServiceSdkConfig() {
		return claimSpiffServiceSdkConfig;
	}

}
