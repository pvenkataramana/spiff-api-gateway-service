package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;

public final class PartnerSaleRegistrationIdFactoryImpl implements PartnerSaleRegistrationIdFactory {

	@Override
	public PartnerSaleRegistrationId construct(
			PartnerSaleRegId partnerSaleRegid) {

		return new PartnerSaleRegistrationIdImpl(partnerSaleRegid.getValue());
	}

}
