package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

public interface ClaimSpiffsServiceAdapterConfigFactory {

	ClaimSpiffsServiceAdapterConfig construct();
}
