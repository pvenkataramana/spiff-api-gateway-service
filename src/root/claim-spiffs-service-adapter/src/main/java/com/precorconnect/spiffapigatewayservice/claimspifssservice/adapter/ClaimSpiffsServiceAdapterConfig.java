package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkConfig;

public interface ClaimSpiffsServiceAdapterConfig {

	ClaimSpiffServiceSdkConfig getClaimSpiffsServiceSdkConfig();
}
