package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;

public interface ClaimSpiffViewFactory {

	ClaimSpiffSynopysisView construct(com.precorconnect.claimspiffservice.objectmodel.ClaimSpiffView claimSpiffView);
}
