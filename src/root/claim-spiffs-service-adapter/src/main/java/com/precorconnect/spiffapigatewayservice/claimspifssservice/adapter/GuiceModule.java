package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final ClaimSpiffsServiceAdapterConfig config;


    /*
    constructors
     */
    public GuiceModule(
            @NonNull ClaimSpiffsServiceAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {

        bindFactories();

        bindFeatures();

    }

    private void bindFeatures() {
        bind(AddClaimSpiffsFeature.class)
        .to(AddClaimSpiffsFeatureImpl.class);

        bind(GetClaimSpiffsWithIdsFeature.class)
        .to(GetClaimSpiffsWithIdsFeatureImpl.class);

        bind(ListEntitlementsWithPartnerIdFeature.class)
        .to(ListEntitlementsWithPartnerIdFeatureImpl.class);

        bind(UpdateInvoiceUrlFeature.class)
        .to(UpdateInvoiceUrlFeatureImpl.class);

    }

    private void bindFactories() {

        bind(ClaimSpiffViewFactory.class)
        .to(ClaimSpiffViewFactoryImpl.class);

    	bind(InvoiceUrlFactory.class)
        .to(InvoiceUrlFactoryImpl.class);

    	bind(PartnerSaleRegistrationIdFactory.class)
        .to(PartnerSaleRegistrationIdFactoryImpl.class);

    	bind(SpiffEntitlementViewFactory.class)
        .to(SpiffEntitlementViewFactoryImpl.class);

    }

    @Provides
    @Singleton
    public ClaimSpiffServiceSdk claimSpiffsServiceSdk() {
        return

                new ClaimSpiffServiceSdkImpl(
                        config.getClaimSpiffsServiceSdkConfig()
                );

    }

    @Provides
    @Singleton
    public ClaimSpiffsServiceAdapterConfig claimSpiffsServiceAdapterConfig() {

        return config;

    }



}
