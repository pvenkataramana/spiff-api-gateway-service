package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.spiffapigatewayservice.core.ClaimSpiffsServiceAdapter;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

public final class ClaimSpiffServiceAdapterImpl implements ClaimSpiffsServiceAdapter {

	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public ClaimSpiffServiceAdapterImpl(
            @NonNull ClaimSpiffsServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }
	@Override
	public Collection<ClaimSpiffId> addCliamSpiffs(@NonNull Collection<ClaimSpiffDto> claimSpiffDto,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException {
        return injector
        .getInstance(AddClaimSpiffsFeature.class)
        .execute(claimSpiffDto, accessToken);
	}
	@Override
	public Collection<ClaimSpiffSynopysisView> getClaimSpiffsWithIds(
			@NonNull Collection<ClaimSpiffId> claimSpiffIds,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException {
        return injector
        .getInstance(GetClaimSpiffsWithIdsFeature.class)
        .execute(claimSpiffIds, accessToken);
	}
	@Override
	public Collection<SpiffEntitlementView> listEntitlementsWithPartnerId(@NonNull AccountId partnerAccountId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		return injector
		        .getInstance(ListEntitlementsWithPartnerIdFeature.class)
		        .execute(partnerAccountId, accessToken);
	}
	@Override
	public void updateInvoiceUrl(@NonNull PartnerSaleRegId partnerSaleRegistrationId, @NonNull InvoiceUrl invoiceUrl,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		injector
		        .getInstance(UpdateInvoiceUrlFeature.class)
		        .execute(partnerSaleRegistrationId, invoiceUrl, accessToken);

	}

}
