package com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter;

import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementView;

public interface SpiffEntitlementViewFactory {

	com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView construct(
			SpiffEntitlementView spiffEntitlementView);
}
