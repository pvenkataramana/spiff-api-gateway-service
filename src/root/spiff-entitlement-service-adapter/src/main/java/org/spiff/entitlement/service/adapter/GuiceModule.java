package org.spiff.entitlement.service.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.DeleteSpiffEntitlementsFeature;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.DeleteSpiffEntitlementsFeatureImpl;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.InvoiceUrlFactory;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.InvoiceUrlFactoryImpl;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ListEntitlementsWithPartnerIdFeature;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.ListEntitlementsWithPartnerIdFeatureImpl;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.PartnerSaleRegistrationIdFactory;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.PartnerSaleRegistrationIdFactoryImpl;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.SpiffEntitlementServiceAdapterConfig;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.SpiffEntitlementViewFactory;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.SpiffEntitlementViewFactoryImpl;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.UpdateInvoiceUrlFeature;
import com.precorconnect.spiffapigatewayservice.claimspifssservice.adapter.UpdateInvoiceUrlFeatureImpl;
import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdk;
import com.precorconnect.spiffentitlementservice.sdk.SpiffEntitlementServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final SpiffEntitlementServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull SpiffEntitlementServiceAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {
        
        bindFactories();

        bindFeatures();

    }

    private void bindFeatures() {
        
    	bind(ListEntitlementsWithPartnerIdFeature.class)
        .to(ListEntitlementsWithPartnerIdFeatureImpl.class);
        
        bind(UpdateInvoiceUrlFeature.class)
        .to(UpdateInvoiceUrlFeatureImpl.class);
        
        bind(DeleteSpiffEntitlementsFeature.class)
        .to(DeleteSpiffEntitlementsFeatureImpl.class);

    }
    
    private void bindFactories() {
    	
    	bind(InvoiceUrlFactory.class)
        .to(InvoiceUrlFactoryImpl.class);
    	
    	bind(PartnerSaleRegistrationIdFactory.class)
        .to(PartnerSaleRegistrationIdFactoryImpl.class);

    	bind(SpiffEntitlementViewFactory.class)
        .to(SpiffEntitlementViewFactoryImpl.class);

    }

    @Provides
    @Singleton
    public SpiffEntitlementServiceSdk spiffEntitlementServiceSdk() {

        return
                new SpiffEntitlementServiceSdkImpl(
                        config.getSpiffEntitlementServiceSdkConfig()
                );

    }
    
    @Provides
    @Singleton
    public SpiffEntitlementServiceAdapterConfig spiffEntitlementServiceAdapterConfig() {

        return config;

    }



}
