package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

public interface PartnerRepAssociationServiceAdapterConfigFactory {

    PartnerRepAssociationServiceAdapterConfig construct();

}
