package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.core.PartnerRepAssociationServiceAdapter;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisView;

public class PartnerRepAssociationServiceAdapterImpl implements
        PartnerRepAssociationServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public PartnerRepAssociationServiceAdapterImpl(
            @NonNull PartnerRepAssociationServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public Collection<PartnerRepAssociationSynopsisView> listPartnerRepAssociationsWithPartnerId(
			@NonNull AccountId partnerId, @NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
        return injector
        .getInstance(ListPartnerRepAssociationsWithPartnerIdFeature.class)
        .execute(partnerId, accessToken);
	}

}
