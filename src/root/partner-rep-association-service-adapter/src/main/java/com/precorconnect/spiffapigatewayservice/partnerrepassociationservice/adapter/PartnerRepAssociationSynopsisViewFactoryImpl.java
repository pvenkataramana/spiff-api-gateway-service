package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import com.precorconnect.partnerrepassociationservice.PartnerRepAssociationSynopsisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisViewImpl;

public final class PartnerRepAssociationSynopsisViewFactoryImpl implements PartnerRepAssociationSynopsisViewFactory {

	@Override
	public com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisView construct(PartnerRepAssociationSynopsisView partnerRepAssocationView) {
		return new PartnerRepAssociationSynopsisViewImpl(partnerRepAssocationView.getId(), 
														 partnerRepAssocationView.getPartnerId(),
														 partnerRepAssocationView.getRepId());
	}


}
