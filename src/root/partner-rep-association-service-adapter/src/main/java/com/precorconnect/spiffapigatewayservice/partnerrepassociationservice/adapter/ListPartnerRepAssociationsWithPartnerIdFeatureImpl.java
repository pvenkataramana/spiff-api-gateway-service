package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepAssociationSynopsisView;

class ListPartnerRepAssociationsWithPartnerIdFeatureImpl
        implements ListPartnerRepAssociationsWithPartnerIdFeature {

	private PartnerRepAssociationServiceSdk partnerRepAssociationServiceSdk;
	
	private PartnerRepAssociationSynopsisViewFactory factory;
	
	@Inject
	public ListPartnerRepAssociationsWithPartnerIdFeatureImpl(final PartnerRepAssociationServiceSdk partnerRepAssociationServiceSdk,
															  final PartnerRepAssociationSynopsisViewFactory factory) {
		this.partnerRepAssociationServiceSdk = guardThat(
                								"partnerRepAssociationServiceSdk",
                								partnerRepAssociationServiceSdk
												).isNotNull()
												.thenGetValue();
		
		
		this.factory = guardThat(
				"factory",
				factory
				).isNotNull()
				.thenGetValue();

	}
	
	@Override
	public Collection<PartnerRepAssociationSynopsisView> execute(
			@NonNull AccountId partnerId, @NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

		
		Collection<PartnerRepAssociationSynopsisView> partnerRepAssocations = new ArrayList<PartnerRepAssociationSynopsisView>();
		
		partnerRepAssocations.addAll(partnerRepAssociationServiceSdk
											.listPartnerRepAssociationsWithPartnerId(partnerId, accessToken)
											.stream()
											.map(factory::construct)
											.collect(Collectors.toList()));
		
		
		return partnerRepAssocations;
	}

   
}
