package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdkConfig;

public interface PartnerRepAssociationServiceAdapterConfig {

    PartnerRepAssociationServiceSdkConfig getPartnerRepAssociationServiceSdkConfig();

}
