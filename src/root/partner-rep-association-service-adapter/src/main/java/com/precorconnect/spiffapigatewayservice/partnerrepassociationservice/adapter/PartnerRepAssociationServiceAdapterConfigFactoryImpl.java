package com.precorconnect.spiffapigatewayservice.partnerrepassociationservice.adapter;

import java.net.MalformedURLException;
import java.net.URL;

import com.precorconnect.partnerrepassociationservice.sdk.PartnerRepAssociationServiceSdkConfigImpl;

/**
 * Constructs {@link PartnerRepAssociationServiceAdapterConfig}
 * from system properties
 */
public class PartnerRepAssociationServiceAdapterConfigFactoryImpl
        implements PartnerRepAssociationServiceAdapterConfigFactory {

    @Override
    public PartnerRepAssociationServiceAdapterConfig construct() {

        return
                new PartnerRepAssociationServiceAdapterConfigImpl(
                        new PartnerRepAssociationServiceSdkConfigImpl(
                                constructPrecorConnectApiBaseUrl()
                        )
                );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return
                    new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
