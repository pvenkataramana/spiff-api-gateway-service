package com.precorconnect.spiffapigatewayservice.webapi;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Instant;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class Factory {

    /*
    fields
     */
    private final Dummy dummy;

    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    /*
    constructors
     */
    public Factory(
            @NonNull Dummy dummy,
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk
    ) {

        this.dummy =
                guardThat(
                        "dummy",
                        dummy
                )
                        .isNotNull()
                        .thenGetValue();

        this.identityServiceIntegrationTestSdk =
                guardThat(
                        "identityServiceIntegrationTestSdk",
                        identityServiceIntegrationTestSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    factory methods
    */
    public OAuth2AccessToken constructValidAppOAuth2AccessToken() {

        AppJwt appJwt =
                new AppJwtImpl(
                        Instant.now().plusSeconds(480),
                        dummy.getUri(),
                        dummy.getUri()
                );

        return
                identityServiceIntegrationTestSdk
                        .getAppOAuth2AccessToken(appJwt);
    }

}
