package com.precorconnect.spiffapigatewayservice.webapi;

import static com.jayway.restassured.RestAssured.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdkImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest({"server.port=0"})
public class SpiffApiGatewayResourceIT {

    /*
    fields
     */
	private final Config config=
						 new ConfigFactory()
    									 .construct();

	Dummy dummy = new Dummy();

    private final Factory factory =
            new Factory(
            		dummy,
                    new IdentityServiceIntegrationTestSdkImpl(
                    			config
                                      .getIdentityServiceJwtSigningKey()
                    )
            );

    @Value("${local.server.port}")
    int port;

    @Before
    public void setUp() {

        RestAssured.port = port;
    }

    @Test
    public void testListPartnerRepsInfoWithAccountId_whenGiveAccountId_shownPartnerRepInfo() {

        given()
                /*.header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )*/
        		.header(
        				"Content-Type",
        				"application/json"
        				)
                .get("/spiff-api-gateway/partnerrepinfo/"+dummy.getAccountId())
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void testListSpiffEntitlementsAccountId_whenGiveAccountId_shownSpiffEntitlementsWithPartnerRepInfo() {

        given()
                /*.header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )*/
        		.header(
        				"Content-Type",
        				"application/json"
        				)
                .get("/spiff-api-gateway/spiffentitlements/"+dummy.getAccountId())
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void testUploadInvoiceForPartnerSaleRegistration_whenGivenPartnerSaleInvoiceReq_shownPartnerSaleInvoiceInfo() {

        given()
                /*.header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )*/
        		.header(
        				"Content-Type",
        				"application/json"
        				)
        		.body(dummy.getUploadPartnerSaleInvoiceReqWebDto())
                .post("/spiff-api-gateway/uploadinvoice")
                .then()
                .assertThat()
                .statusCode(200);

    }

    @Test
    public void testClaimSpiffEntitlements_whenGivenSpiffEntitlementWithPartnerRepInfo_shownClaimSpiffView() {

        given()
                /*.header(
                        "Authorization",
                        String.format(
                                "Bearer %s",
                                factory
                                        .constructValidAppOAuth2AccessToken()
                                        .getValue()
                        )
                )*/
        		.header(
        				"Content-Type",
        				"application/json"
        				)
        		.body(dummy.getSpiffEntitlementWithPartnerRepInfoWebDtos())
                .post("/spiff-api-gateway/claimspiffentitlements/"+dummy.getAccountId())
                .then()
                .assertThat()
                .statusCode(200);

    }


}
