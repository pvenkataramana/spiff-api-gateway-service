package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerSaleInvoiceWebView;

public interface PartnerSaleInvoiceWebResponseFactory {

	PartnerSaleInvoiceWebView construct(
								@NonNull PartnerSaleInvoiceView partnerSaleInvoiceView
								);

}
