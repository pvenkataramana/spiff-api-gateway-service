package com.precorconnect.spiffapigatewayservice.webapi;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebView;

@Component
public class SpiffEntitlementWithPartnerRepInfoWebResponseFactoryImpl implements
		SpiffEntitlementWithPartnerRepInfoWebResponseFactory {

	@Override
	public SpiffEntitlementWithPartnerRepInfoWebView construct(
			@NonNull SpiffEntitlementWithPartnerRepInfoView spiffEntitlementWithPartnerRepInfoView
			) {

		Long spiffEntitlementId =
					spiffEntitlementWithPartnerRepInfoView
							.getSpiffEntitlementId()
							.getValue();

		Long partnerSaleRegistrationId =
				spiffEntitlementWithPartnerRepInfoView
						.getPartnerSaleRegistrationId()
						.getValue();

		DateFormat dateFormat_install = new SimpleDateFormat("MM/dd/yyyy");

		String installDate =
				dateFormat_install.format(
						spiffEntitlementWithPartnerRepInfoView
											.getInstallDate()
											.getValue()
										);

		Double spiffAmount =
				spiffEntitlementWithPartnerRepInfoView
						.getSpiffAmount()
						.getValue();


		PartnerRepInfoView partnerRepInfoView = spiffEntitlementWithPartnerRepInfoView.getPartnerRepInfoView();


		String partnerRepUserId = partnerRepInfoView!= null ? partnerRepInfoView.getUserId().getValue() : null;

		String firstName = partnerRepInfoView!= null ? partnerRepInfoView.getFirstName() : null;

		String lastName = partnerRepInfoView!= null ? partnerRepInfoView.getLastName() : null;

		boolean isBankInfoExists = partnerRepInfoView != null ? partnerRepInfoView.isBankInfoExists() : false;

		boolean isW9InfoExists =  partnerRepInfoView != null ? partnerRepInfoView.isW9InfoExists() : false;

		boolean isContactInfoExists = partnerRepInfoView != null ? partnerRepInfoView.isContactInfoExists() : false;

		String invoiceUrl =
				spiffEntitlementWithPartnerRepInfoView
						.getInvoiceInfo()
						.getInvoiceUrl()
						.getValue();

		String invoiceNumber =
				spiffEntitlementWithPartnerRepInfoView
						.getInvoiceInfo()
						.getInvoiceNumber()
						.getValue();

		String facilityName =
				spiffEntitlementWithPartnerRepInfoView
						.getFacilityName()
						.getValue();

		String sellDate =
				dateFormat_install.format(
						spiffEntitlementWithPartnerRepInfoView
											.getSellDate()
											.getValue()
										);

		return
				new SpiffEntitlementWithPartnerRepInfoWebView(
						spiffEntitlementId,
						partnerSaleRegistrationId,
						installDate,
						spiffAmount,
						partnerRepUserId,
						firstName,
						lastName,
						isBankInfoExists,
						isW9InfoExists,
						isContactInfoExists,
						invoiceUrl,
						invoiceNumber,
						facilityName,
						sellDate
						);
	}

}
