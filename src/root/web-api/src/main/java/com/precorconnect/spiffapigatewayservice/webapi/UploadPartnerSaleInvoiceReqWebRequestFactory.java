package com.precorconnect.spiffapigatewayservice.webapi;

import org.springframework.web.multipart.MultipartFile;

import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface UploadPartnerSaleInvoiceReqWebRequestFactory {

	UploadPartnerSaleInvoiceReq construct(String partnerSaleInvoiceNumber, Long partnerSaleRegId, MultipartFile file);

}
