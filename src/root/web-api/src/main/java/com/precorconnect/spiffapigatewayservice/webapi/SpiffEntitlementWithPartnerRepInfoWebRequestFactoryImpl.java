package com.precorconnect.spiffapigatewayservice.webapi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.PartnerSaleRegId;
import com.precorconnect.PartnerSaleRegIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityName;
import com.precorconnect.spiffapigatewayservice.objectmodel.FacilityNameImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.InstallDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceInfoImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SellDate;
import com.precorconnect.spiffapigatewayservice.objectmodel.SellDateImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmount;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementId;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementIdImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementViewImpl;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebDto;

@Component
public class SpiffEntitlementWithPartnerRepInfoWebRequestFactoryImpl implements
		SpiffEntitlementWithPartnerRepInfoWebRequestFactory {

	@Override
	public SpiffEntitlementView construct(
			@NonNull SpiffEntitlementWithPartnerRepInfoWebDto spiffEntitlementWithPartnerRepInfoWebDto) {

		SpiffEntitlementId spiffEntitlementId =
								new SpiffEntitlementIdImpl(
														spiffEntitlementWithPartnerRepInfoWebDto
															.getSpiffEntitlementId()
														);

		PartnerSaleRegId partnerSaleRegistrationId =
										new PartnerSaleRegIdImpl(
												spiffEntitlementWithPartnerRepInfoWebDto
													.getPartnerSaleRegistrationId()
												);

		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		Date date_install;
		try {
			date_install = formatter
								.parse(
										spiffEntitlementWithPartnerRepInfoWebDto
											.getInstallDate()
										);
		} catch (ParseException e) {
			throw new RuntimeException("Exception occured in parsing the date format: ",e);
		}

		InstallDate installDate =
						new InstallDateImpl(
										date_install
										);

		SpiffAmount spiffAmount =
						new SpiffAmountImpl(
								spiffEntitlementWithPartnerRepInfoWebDto
									.getSpiffAmount()
								);

		UserId userId =
							new UserIdImpl(
									spiffEntitlementWithPartnerRepInfoWebDto
													.getPartnerRepUserId()
										);

		InvoiceInfo invoiceInfo =
						new InvoiceInfoImpl(
									new InvoiceUrlImpl(
											spiffEntitlementWithPartnerRepInfoWebDto
												.getInvoiceUrl()
												),
									new InvoiceNumberImpl(
											spiffEntitlementWithPartnerRepInfoWebDto
												.getInvoiceNumber()
												)
								);

		FacilityName facilityName =
						new FacilityNameImpl(
									spiffEntitlementWithPartnerRepInfoWebDto
										.getFacilityName()
									);

		Date date_sell;
		try {
			date_sell = formatter
								.parse(
										spiffEntitlementWithPartnerRepInfoWebDto
											.getSellDate()
										);
		} catch (ParseException e) {
			throw new RuntimeException("Exception occured in parsing the date format: ",e);
		}

		SellDate sellDate =
						new SellDateImpl(
										date_sell
										);

		return
				new SpiffEntitlementViewImpl(
						partnerSaleRegistrationId,
						installDate,
						spiffAmount,
						userId,
						invoiceInfo,
						facilityName,
						spiffEntitlementId,
						sellDate
						);
	}

}
