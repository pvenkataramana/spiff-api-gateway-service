package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.SpiffEntitlementWithPartnerRepInfoWebView;

public interface SpiffEntitlementWithPartnerRepInfoWebResponseFactory {

	SpiffEntitlementWithPartnerRepInfoWebView construct(
												@NonNull SpiffEntitlementWithPartnerRepInfoView spiffEntitlementWithPartnerRepInfoView
												);

}
