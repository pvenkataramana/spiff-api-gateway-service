package com.precorconnect.spiffapigatewayservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffgatewayservice.webapiobjectmodel.PartnerRepInfoWebView;

public interface PartnerRepInfoWebResponseFactory {

	PartnerRepInfoWebView construct(
							@NonNull PartnerRepInfoView partnerRepInfoView
							);

}
