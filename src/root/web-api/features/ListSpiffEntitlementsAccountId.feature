Feature: List Spiff Entitlements AccountId
  List Spiff Entitlements AccountId

  Background:
    Given a accountId consists of:
      | attribute     				| validation | type   |
      | accountId     				| required   | string |
      
  Scenario: Success
    Given I provide an accessToken identifying me as a partner rep
    And provide a valid accountId
    When I execute listSpiffEntitlementsAccountId
    Then the collection of SpiffEntitlementWithPartnerRepInfoWebView with the matched accountid are returned

