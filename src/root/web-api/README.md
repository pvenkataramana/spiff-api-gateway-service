## Description
Precor Connect spiff api gateway service API for ReST.

## Features

##### List Partner Reps Info With AccountId
* [documentation](features/ListPartnerRepsInfoWithAccountId.feature)

##### List SpiffEntitlements AccountId
* [documentation](features/ListSpiffEntitlementsAccountId.feature)

##### Upload Invoice For PartnerSaleRegistration
* [documentation](features/UploadInvoiceForPartnerSaleRegistration.feature)

##### ClaimSpiffEntitlements
* [documentation](features/ClaimSpiffEntitlements.feature)


## API Explorer

##### Environments:
-  [dev](https://spiff-api-gateway-service-dev.precorconnect.com/)
-  [qa](https://spiff-api-gateway-service-qa.precorconnect.com/)
-  [prod](https://spiff-api-gateway-service-prod.precorconnect.com/)