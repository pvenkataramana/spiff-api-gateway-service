package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;

import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface Core {

	Collection<PartnerRepInfoView> listPartnerRepsInfoWithPartnerAccountId(
			AccountId partnerAccountId, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	Collection<SpiffEntitlementWithPartnerRepInfoView> listSpiffEntitlementsWithPartnerAccountId(
			AccountId partnerAccountId, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	PartnerSaleInvoiceView uploadInvoiceForPartnerSaleRegistration(
			final UploadPartnerSaleInvoiceReq req, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

	Collection<ClaimSpiffView> claimSpiffEntitlements(AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException;

}
