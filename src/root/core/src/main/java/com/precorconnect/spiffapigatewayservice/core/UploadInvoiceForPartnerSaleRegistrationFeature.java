package com.precorconnect.spiffapigatewayservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerSaleInvoiceView;
import com.precorconnect.spiffapigatewayservice.objectmodel.UploadPartnerSaleInvoiceReq;

public interface UploadInvoiceForPartnerSaleRegistrationFeature {

	PartnerSaleInvoiceView execute(
									   @NonNull UploadPartnerSaleInvoiceReq req,
									   @NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException;

}
