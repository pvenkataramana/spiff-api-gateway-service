package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;

class GuiceModule extends
        AbstractModule {


    private final PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter;

    private final PartnerRepServiceAdapter partnerRepServiceAdapter;

    private final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter;

    private final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter;


    public GuiceModule(
            final @NonNull PartnerSaleInvoiceServiceAdapter partnerSaleInvoiceServiceAdapter,
            final @NonNull PartnerRepServiceAdapter partnerRepServiceAdapter,
            final @NonNull PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter,
            final @NonNull ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter
    ) {

        this.partnerSaleInvoiceServiceAdapter =
                guardThat(
                        "partnerSaleInvoiceServiceAdapter",
                        partnerSaleInvoiceServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

        this.partnerRepServiceAdapter =
                guardThat(
                        "partnerRepServiceAdapter",
                        partnerRepServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

        this.partnerRepAssociationServiceAdapter =
                guardThat(
                        "partnerRepAssociationServiceAdapter",
                        partnerRepAssociationServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

        this.claimSpiffsServiceAdapter = guardThat(
                "claimSpiffsServiceAdapter",
                claimSpiffsServiceAdapter
        		)
                .isNotNull()
                .thenGetValue();


    }

    @Override
    protected void configure() {

        bind(PartnerSaleInvoiceServiceAdapter.class)
                .toInstance(partnerSaleInvoiceServiceAdapter);

        bind(PartnerRepServiceAdapter.class)
                .toInstance(partnerRepServiceAdapter);

        bind(ClaimSpiffsServiceAdapter.class)
        .toInstance(claimSpiffsServiceAdapter);

        bind(PartnerRepAssociationServiceAdapter.class)
                .toInstance(partnerRepAssociationServiceAdapter);

        bindFeatures();
        bindFactories();

    }

    private void bindFeatures() {

        bind(ListPartnerRepsInfoWithAccountIdFeature.class)
                .to(ListPartnerRepsInfoWithAccountIdFeatureImpl.class);

        bind(ListSpiffEntitlementsWithPartnerAccountIdFeature.class)
                .to(ListSpiffEntitlementsWithPartnerAccountIdFeatureImpl.class);

        bind(UploadInvoiceForPartnerSaleRegistrationFeature.class)
                .to(UploadInvoiceForPartnerSaleRegistrationFeatureImpl.class);

        bind(ClaimSpiffEntitlementsFeature.class)
        .to(ClaimSpiffEntitlementsFeatureImpl.class);



    }

    private void bindFactories(){
    	 bind(ClaimSpiffDtoFactory.class)
         .to(ClaimSpiffDtoFactoryImpl.class);

    }
}
