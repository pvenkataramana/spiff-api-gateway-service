package com.precorconnect.spiffapigatewayservice.core;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.PartnerRepAccessContext;

public interface IdentityServiceAdapter {
    PartnerRepAccessContext getPartnerRepAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;


}
