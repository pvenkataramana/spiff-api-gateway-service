package com.precorconnect.spiffapigatewayservice.core;

import java.util.Collection;
import java.util.List;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;

public interface FindPartnerRepsInfoWithAccountIdHelper {

	Collection<PartnerRepInfoView> execute(PartnerRepServiceAdapter partnerRepServiceAdapter,
										   PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter,
										   AccountId partnerAccountId,
										   OAuth2AccessToken accessToken);

	Collection<PartnerRepInfoView> execute(PartnerRepServiceAdapter partnerRepServiceAdapter,
			   							   List<UserId> partnerRepUserIds,
			   							   OAuth2AccessToken accessToken);

}
