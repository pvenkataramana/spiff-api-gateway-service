package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffId;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffSynopysisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffView;
import com.precorconnect.spiffapigatewayservice.objectmodel.ClaimSpiffViewImpl;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;

@Singleton
public final class ClaimSpiffEntitlementsFeatureImpl implements ClaimSpiffEntitlementsFeature {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClaimSpiffEntitlementsFeatureImpl.class);

	private final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter;

	private final ClaimSpiffDtoFactory claimSpiffDtoFactory;

	private final PartnerRepServiceAdapter partnerRepServiceAdapter;


	@Inject
	public ClaimSpiffEntitlementsFeatureImpl(
			@NonNull final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter,
			@NonNull final PartnerRepServiceAdapter partnerRepServiceAdapter,
			@NonNull final ClaimSpiffDtoFactory claimSpiffDtoFactory) {

		this.claimSpiffsServiceAdapter = guardThat(
                "claimSpiffsServiceAdapter",
                claimSpiffsServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		this.claimSpiffDtoFactory = guardThat(
                "claimSpiffDtoFactory",
                claimSpiffDtoFactory
			  ).isNotNull()
			   .thenGetValue();

		this.partnerRepServiceAdapter = guardThat(
                "partnerRepServiceAdapter",
                partnerRepServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

	}

	@Override
	public Collection<ClaimSpiffView> execute(AccountId partnerAccountId,
			Collection<SpiffEntitlementView> spiffEntitlements, OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		Collection<ClaimSpiffDto> claimSpiffDtos = new ArrayList<ClaimSpiffDto>();

		for(SpiffEntitlementView entitlement : spiffEntitlements) {
			claimSpiffDtos.add(claimSpiffDtoFactory.construct(partnerAccountId, entitlement));
		}

		LOGGER.debug(String.format("recieved %s spiff entitlements for claim", spiffEntitlements.size()));

		List<UserId> partnerRepIds = spiffEntitlements.stream()
				   									  .map(entitlement -> entitlement.getPartnerRepuserId())
				   									  .collect(Collectors.toList());


		Collection<ClaimSpiffId> claimSpiffIds = claimSpiffsServiceAdapter.addCliamSpiffs(claimSpiffDtos, accessToken);

		LOGGER.debug(String.format("recieved %s claim ids", claimSpiffIds.size()));

		List<ClaimSpiffSynopysisView> claimSpiffSynopsysViews = claimSpiffsServiceAdapter.getClaimSpiffsWithIds(claimSpiffIds, accessToken)
																					   .stream()
																					   .sorted((view1, view2) -> view1.getPartnerRepUserId().getValue().compareTo(view2.getPartnerRepUserId().getValue()))
																					   .collect(Collectors.toList());

		LOGGER.debug(String.format("recieved %s claim views", claimSpiffSynopsysViews.size()));

		List<PartnerRepSynopsisView> partnerRepViews = partnerRepServiceAdapter.getPartnerRepsWithIds(partnerRepIds, accessToken)
															 						 .stream()
															 						 .sorted((view1, view2) -> view1.getId().getValue().compareTo(view2.getId().getValue()))
															 						 .collect(Collectors.toList());

		LOGGER.debug(String.format("recieved %s partner views", partnerRepViews.size()));


		Collection<ClaimSpiffView> claimSpiffViews = new ArrayList<ClaimSpiffView>();

		Map<String, PartnerRepSynopsisView> userIdToPartnerRepSynposysViewMap = new HashMap<String, PartnerRepSynopsisView>();

		for(int i=0; i < claimSpiffSynopsysViews.size(); i++) {


			UserId partnerRepUserId = claimSpiffSynopsysViews.get(i).getPartnerRepUserId();

			PartnerRepSynopsisView partnerRepSynopsysView = userIdToPartnerRepSynposysViewMap.get(partnerRepUserId.getValue());

			if (partnerRepSynopsysView == null) {
				LOGGER.debug(String.format("calling partner rep service for rep id %s ", partnerRepUserId.getValue()));

				partnerRepSynopsysView = partnerRepServiceAdapter
															.getPartnerRepsWithIds(Arrays.asList(partnerRepUserId), accessToken)
															.stream()
															.collect(Collectors.toList())
															.get(0);
				userIdToPartnerRepSynposysViewMap.put(partnerRepUserId.getValue(), partnerRepSynopsysView);
			}



			ClaimSpiffSynopysisView claimSpiffSynopsysView = claimSpiffSynopsysViews.get(i);

			ClaimSpiffView claimSpiffview = new ClaimSpiffViewImpl(claimSpiffSynopsysView.getClaimId(),
																   claimSpiffSynopsysView.getPartnerSaleRegistrationId(),
																   claimSpiffSynopsysView.getPartnerAccountId(),
																   claimSpiffSynopsysView.getPartnerRepUserId(),
																   claimSpiffSynopsysView.getInstallDate(),
																   claimSpiffSynopsysView.getSpiffAmount(),
																   claimSpiffSynopsysView.getSpiffClaimedDate(),
																   claimSpiffSynopsysView.getFacilityName(),
																   claimSpiffSynopsysView.getInvoiceNumber(),
																   partnerRepSynopsysView.getFirstName(),
																   partnerRepSynopsysView.getLastName());

			claimSpiffViews.add(claimSpiffview);
		}


		return claimSpiffViews;
	}

}
