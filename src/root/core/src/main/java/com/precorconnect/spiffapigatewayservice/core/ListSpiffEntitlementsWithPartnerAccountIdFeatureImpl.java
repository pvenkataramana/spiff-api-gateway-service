package com.precorconnect.spiffapigatewayservice.core;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AccountId;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoView;
import com.precorconnect.spiffapigatewayservice.objectmodel.SpiffEntitlementWithPartnerRepInfoViewImpl;

public final class ListSpiffEntitlementsWithPartnerAccountIdFeatureImpl
		implements ListSpiffEntitlementsWithPartnerAccountIdFeature {

	private final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter;

	private final PartnerRepServiceAdapter partnerRepServiceAdapter;

	private final FindPartnerRepsInfoWithAccountIdHelper findPartnerRepsInfoWithAccountIdHelper;


	@Inject
	public ListSpiffEntitlementsWithPartnerAccountIdFeatureImpl(
			@NonNull final ClaimSpiffsServiceAdapter claimSpiffsServiceAdapter,
			@NonNull final PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter,
			@NonNull final PartnerRepServiceAdapter partnerRepServiceAdapter) {

		this.claimSpiffsServiceAdapter = guardThat(
                "claimSpiffsServiceAdapter",
                claimSpiffsServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		this.partnerRepServiceAdapter = guardThat(
                "partnerRepServiceAdapter",
                partnerRepServiceAdapter
			  ).isNotNull()
			   .thenGetValue();

		findPartnerRepsInfoWithAccountIdHelper = new FindPartnerRepsInfoWithAccountIdHelperImpl();

	}


	@Override
	public Collection<SpiffEntitlementWithPartnerRepInfoView> execute(final AccountId partnerAccountId,
													                  final OAuth2AccessToken accessToken) throws AuthenticationException , AuthorizationException{


		List<SpiffEntitlementView> spiffEntitlements = claimSpiffsServiceAdapter
				.listEntitlementsWithPartnerId(partnerAccountId, accessToken)
				.stream()
				.collect(Collectors.toList());

		List<UserId> entitlementPartnerRepUserIds = spiffEntitlements
													.stream()
													.filter(entitlement -> entitlement.getPartnerRepuserId() != null)
													.map(entitlement -> entitlement.getPartnerRepuserId())
													.distinct()
													.collect(Collectors.toList());

		List<PartnerRepInfoView> partnerRepsInfoSortedByRepId = findPartnerRepsInfoWithAccountIdHelper
				.execute(partnerRepServiceAdapter,
						 entitlementPartnerRepUserIds,
						 accessToken)
				.stream()
				.collect(Collectors.toList());


		Collection<SpiffEntitlementWithPartnerRepInfoView> entitlementsWithPartnerRepInfo = new ArrayList<SpiffEntitlementWithPartnerRepInfoView>();

		for (SpiffEntitlementView view : spiffEntitlements) {

			if (view.getPartnerRepuserId() != null) {
				PartnerRepInfoView partnerRepInfoView = findPartnerRepInfoViewForPartnerRepId(
						view.getPartnerRepuserId(),
						partnerRepsInfoSortedByRepId);

				SpiffEntitlementWithPartnerRepInfoView entitlementWithPartnerRepInfo = new SpiffEntitlementWithPartnerRepInfoViewImpl(
						view.getPartnerSaleRegistrationId(),
						view.getInstallDate(), view.getSpiffAmount(),
						view.getInvoiceInfo(), view.getFacilityName(),
						view.getSpiffEntitlementId(), partnerRepInfoView,
						view.getSellDate());
				entitlementsWithPartnerRepInfo
						.add(entitlementWithPartnerRepInfo);

			} else {
				SpiffEntitlementWithPartnerRepInfoView entitlementWithPartnerRepInfo = new SpiffEntitlementWithPartnerRepInfoViewImpl(
						view.getPartnerSaleRegistrationId(),
						view.getInstallDate(), view.getSpiffAmount(),
						view.getInvoiceInfo(), view.getFacilityName(),
						view.getSpiffEntitlementId(), null,
						view.getSellDate());
				entitlementsWithPartnerRepInfo
						.add(entitlementWithPartnerRepInfo);
			}

		}

		return entitlementsWithPartnerRepInfo;
	}

	private PartnerRepInfoView findPartnerRepInfoViewForPartnerRepId(final UserId partnerRepId, final List<PartnerRepInfoView> partnerRepInfoViews) {
		for(PartnerRepInfoView view: partnerRepInfoViews) {
			if(view.getUserId().getValue().equalsIgnoreCase(partnerRepId.getValue())) {
				return view;
			}
		}

		return null;
	}

}
