package com.precorconnect.spiffapigatewayservice.core;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepInfoView;



@RunWith(MockitoJUnitRunner.class)
public class ListPartnerRepsInfoWithAccountIdFeatureImplTest {

	/*
    fields
     */
	@Mock
	private  PartnerRepServiceAdapter partnerRepServiceAdapter;

	@Mock
	private  PartnerRepAssociationServiceAdapter partnerRepAssociationServiceAdapter;

	@Mock
	private  FindPartnerRepsInfoWithAccountIdHelper findPartnerRepsInfoWithAccountIdHelper;

	private Dummy dummy = new Dummy();

	@InjectMocks
	private ListPartnerRepsInfoWithAccountIdFeatureImpl listPartnerRepsInfoWithAccountIdFeatureImpl;

	@Test
	@Ignore
	public void testListPartnerRepsInfoWithAccountId_whenAccountId_shownPartnerRepInfoViewList(
			) throws AuthenticationException, AuthorizationException{

		when(
				findPartnerRepsInfoWithAccountIdHelper
				.execute(
						partnerRepServiceAdapter,
						partnerRepAssociationServiceAdapter,
						dummy.getAccountId(),
						dummy.getAccessToken()
						)
				)
		.thenReturn(
				dummy.getListPartnerRepInfoView()
				);

		Collection<PartnerRepInfoView> listPartnerRepInfoView=listPartnerRepsInfoWithAccountIdFeatureImpl
																	.execute(
																			dummy.getAccountId(),
																			dummy.getAccessToken()
																	);

		assertThat(listPartnerRepInfoView.size())
		.isGreaterThan(0);
	}

}
