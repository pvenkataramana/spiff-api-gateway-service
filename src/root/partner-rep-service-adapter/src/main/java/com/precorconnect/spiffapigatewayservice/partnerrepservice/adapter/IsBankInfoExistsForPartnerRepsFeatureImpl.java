package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;

public final class IsBankInfoExistsForPartnerRepsFeatureImpl implements IsBankInfoExistsForPartnerRepsFeature {

	private static final Logger LOGGER = LoggerFactory.getLogger(IsBankInfoExistsForPartnerRepsFeatureImpl.class);

	private final PartnerRepServiceSdk partnerRepServiceSdk;

	private final MostRecentlyCommitedBankInfoFactory factory;

	@Inject
	public IsBankInfoExistsForPartnerRepsFeatureImpl(final PartnerRepServiceSdk partnerRepServiceSdk,
			                                final MostRecentlyCommitedBankInfoFactory factory) {
		this.partnerRepServiceSdk = guardThat(
                "partnerRepServiceSdk",
                partnerRepServiceSdk
			  ).isNotNull()
			   .thenGetValue();

		this.factory = guardThat(
                "mostRecentlyCommitedBankInfoFactory",
                factory
			  ).isNotNull()
			   .thenGetValue();
	}
	@Override
	public Collection<MostRecentlyCommitedBankInfo> execute(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		Collection<MostRecentlyCommitedBankInfo> mostRecentlyCommittedBankInfo = new ArrayList<MostRecentlyCommitedBankInfo>();

		LOGGER.debug("inside IsBankInfoExistsForPartnerRepsFeature execute");
		mostRecentlyCommittedBankInfo.addAll(partnerRepServiceSdk.isBankInfoExistsForPartnerReps(userIds, accessToken)
						    									 .stream()
						    									 .map(factory::construct)
						    									 .collect(Collectors.toList()));

		return mostRecentlyCommittedBankInfo;
	}

}
