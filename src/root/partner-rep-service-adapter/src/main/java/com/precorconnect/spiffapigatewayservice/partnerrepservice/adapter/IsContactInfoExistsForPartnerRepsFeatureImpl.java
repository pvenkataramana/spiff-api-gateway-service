package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepContactInfo;

public final class IsContactInfoExistsForPartnerRepsFeatureImpl implements IsContactInfoExistsForPartnerRepsFeature {

	
private final PartnerRepServiceSdk partnerRepServiceSdk;
	
	private final PartnerRepContactInfoFactory factory;

	@Inject
	public IsContactInfoExistsForPartnerRepsFeatureImpl(final PartnerRepServiceSdk partnerRepServiceSdk,
			                                final PartnerRepContactInfoFactory factory) {
		this.partnerRepServiceSdk = guardThat(
                "partnerRepServiceSdk",
                partnerRepServiceSdk
			  ).isNotNull()
			   .thenGetValue();
		
		this.factory = guardThat(
                "partnerRepContactInfoFactory",
                factory
			  ).isNotNull()
			   .thenGetValue();
	}
	
	@Override
	public Collection<PartnerRepContactInfo> execute(
			@NonNull Collection<UserId> userIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {

		Collection<PartnerRepContactInfo> partnerRepContactInfo = new ArrayList<PartnerRepContactInfo>();
		
		partnerRepContactInfo.addAll(partnerRepServiceSdk.searchForPartnerRepContactInfoWithIds(userIds, accessToken)
						    									 .stream()
						    									 .map(factory::construct)
						    									 .collect(Collectors.toList()));

		return partnerRepContactInfo;
	}

}
