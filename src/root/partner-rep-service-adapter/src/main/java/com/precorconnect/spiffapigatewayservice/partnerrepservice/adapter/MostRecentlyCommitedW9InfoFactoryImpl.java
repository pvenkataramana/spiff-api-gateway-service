package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.partnerrepservice.MostRecentlyCommitedW9nfoDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9Info;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedW9InfoImpl;

public final class MostRecentlyCommitedW9InfoFactoryImpl implements MostRecentlyCommitedW9InfoFactory {

	@Override
	public MostRecentlyCommitedW9Info construct(MostRecentlyCommitedW9nfoDto mostRecentlyCommittedW9InfoDto) {
		return new MostRecentlyCommitedW9InfoImpl(mostRecentlyCommittedW9InfoDto.getUserId(),
				mostRecentlyCommittedW9InfoDto.IsInfoExists());
	}

}
