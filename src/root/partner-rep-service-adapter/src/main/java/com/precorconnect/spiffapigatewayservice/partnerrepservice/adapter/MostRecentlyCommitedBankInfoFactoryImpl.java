package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.partnerrepservice.MostRecentlyCommitedBankInfoDto;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfo;
import com.precorconnect.spiffapigatewayservice.objectmodel.MostRecentlyCommitedBankInfoImpl;

public final class MostRecentlyCommitedBankInfoFactoryImpl implements MostRecentlyCommitedBankInfoFactory {

	@Override
	public MostRecentlyCommitedBankInfo construct(MostRecentlyCommitedBankInfoDto mostRecentlyCommittedBankInfoDto) {
		return new MostRecentlyCommitedBankInfoImpl(mostRecentlyCommittedBankInfoDto.getUserId(),
				mostRecentlyCommittedBankInfoDto.IsInfoExists());
	}

}
