package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public interface GetPartnerRepsWithIdsFeature {
	
	Collection<PartnerRepSynopsisView> execute(@NonNull Collection<UserId> partnerRepIds,
											   @NonNull OAuth2AccessToken accessToken)
			                                   throws AuthenticationException, AuthorizationException;

}
