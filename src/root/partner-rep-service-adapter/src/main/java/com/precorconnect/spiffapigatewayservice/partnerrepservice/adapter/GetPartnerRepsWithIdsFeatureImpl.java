package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.spiffapigatewayservice.objectmodel.PartnerRepSynopsisView;

public final class GetPartnerRepsWithIdsFeatureImpl implements GetPartnerRepsWithIdsFeature {
	
	private final PartnerRepServiceSdk partnerRepServiceSdk;
	
	private final PartnerRepSynopsisViewFactory partnerRepSynopsisViewFactory;
	
	@Inject
	public GetPartnerRepsWithIdsFeatureImpl(final PartnerRepServiceSdk partnerRepServiceSdk,
			                                final PartnerRepSynopsisViewFactory partnerRepSynopsisViewFactory) {
		this.partnerRepServiceSdk = guardThat(
                "partnerRepServiceSdk",
                partnerRepServiceSdk
			  ).isNotNull()
			   .thenGetValue();
		
		this.partnerRepSynopsisViewFactory = guardThat(
                "partnerRepSynopsisViewFactory",
                partnerRepSynopsisViewFactory
			  ).isNotNull()
			   .thenGetValue();
	}
	

	@Override
	public Collection<PartnerRepSynopsisView> execute(
			@NonNull Collection<UserId> partnerRepIds,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		
		Collection<PartnerRepSynopsisView> partnerReps = new ArrayList<PartnerRepSynopsisView>();
		
		partnerReps.addAll(partnerRepServiceSdk.getPartnerRepsWithIds(partnerRepIds, accessToken)
							.stream()
							.map(partnerRepSynopsisViewFactory::construct)
							.collect(Collectors.toList()));
		
		return partnerReps;
	}

}
