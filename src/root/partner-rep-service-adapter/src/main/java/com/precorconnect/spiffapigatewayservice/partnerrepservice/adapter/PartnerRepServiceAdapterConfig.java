package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkConfig;

public interface PartnerRepServiceAdapterConfig {

   PartnerRepServiceSdkConfig getPartnerRepServiceSdkConfig();

}
