package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkConfig;

public final class PartnerRepServiceAdapterConfigImpl implements PartnerRepServiceAdapterConfig {

	private final PartnerRepServiceSdkConfig partnerRepServiceSdkConfig;
	
	public PartnerRepServiceAdapterConfigImpl(final PartnerRepServiceSdkConfig partnerRepServiceSdkConfig) {
		this.partnerRepServiceSdkConfig =  guardThat(
                							"partnerRepServiceSdkConfig",
                							partnerRepServiceSdkConfig
											)
											 .isNotNull()
											 .thenGetValue();

	}
	
	@Override
	public PartnerRepServiceSdkConfig getPartnerRepServiceSdkConfig() {
		return partnerRepServiceSdkConfig;
	}

}
