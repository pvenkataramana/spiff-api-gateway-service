package com.precorconnect.spiffapigatewayservice.partnerrepservice.adapter;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final PartnerRepServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull PartnerRepServiceAdapterConfig config
    ) {

        this.config =
                guardThat(
                        "config",
                        config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure() {
        
        bindFactories();

        bindFeatures();

    }

    private void bindFeatures() {

        bind(GetPartnerRepsWithIdsFeature.class)
         .to(GetPartnerRepsWithIdsFeatureImpl.class);
        
        bind(IsBankInfoExistsForPartnerRepsFeature.class)
        .to(IsBankInfoExistsForPartnerRepsFeatureImpl.class);

        bind(IsW9InfoExistsForPartnerRepsFeature.class)
        .to(IsW9InfoExistsForPartnerRepsFeatureImpl.class);

        bind(IsContactInfoExistsForPartnerRepsFeature.class)
        .to(IsContactInfoExistsForPartnerRepsFeatureImpl.class);

    }
    
    private void bindFactories() {
        bind(MostRecentlyCommitedBankInfoFactory.class)
        .to(MostRecentlyCommitedBankInfoFactoryImpl.class);

        bind(MostRecentlyCommitedW9InfoFactory.class)
        .to(MostRecentlyCommitedW9InfoFactoryImpl.class);

        bind(PartnerRepContactInfoFactory.class)
        .to(PartnerRepContactInfoFactoryImpl.class);
        
        bind(PartnerRepSynopsisViewFactory.class)
        .to(PartnerRepSynopsisViewFactoryImpl.class);

    }

    @Provides
    @Singleton
    public PartnerRepServiceSdk partnerRepServiceSdk() {

        return
                new PartnerRepServiceSdkImpl(
                        config.getPartnerRepServiceSdkConfig()
                );

    }


}
